class RayCast {
    constructor(offsetX, offsetY, w, h) {
        this.offset = {x: offsetX, y:offsetY};
        this.size = {w: w, h:h, hd2:Math.floor(h/2) };
        this.maxDistance = 320;
        this.maxWallHeight = this.size.h;
    }
    draw(car) {
        this.car = car;

        push();
        translate(this.offset.x, this.offset.y+this.size.hd2);
        this._performDrawing();
        pop();
    }

    _performDrawing() {
        stroke(255,0,0);
        fill(204);
        rect(0, -this.size.hd2, this.size.w, this.size.h);

        fill(70);
        noStroke();
        rect(0, 0, this.size.w, this.size.hd2);

        noStroke();
        const w = this.size.w / this.car.raysAngles.length;
        let x = 0;
        for (let i=0; i<this.car.raysAngles.length; i++,x+=w) {
            let h = this._getWallHeight(this.car.raysDistance[i]);

            // h /= Math.cos(this.car.raysAngles[i]);

            fill(this._getColor(h));
            rect(x, -h/2, w, h);
        }
    }

    _getColor(h) {
        return p5.prototype.map(h, 0, this.maxWallHeight, 50, 150);
    }

    _getWallHeight(d) {
        const newH = p5.prototype.map(d, 0, this.maxDistance, this.maxWallHeight, 0);
        return Math.max(newH, 3);
    }


}
