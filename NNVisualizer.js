class NNVisualizer {
    constructor(offsetX, offsetY, w, h, normBrain) {
        this.nnDimmension = {
            inputs: normBrain.weigths_inputHidden.rows,
            hidden: normBrain.weigths_inputHidden.cols,
            output: normBrain.weigths_hiddenOutput.cols,
        };
        this.radius = 5;
        this.padding = 10;
        this.offset = {
            x: offsetX + this.padding,
            y: offsetY + this.padding
        };
        this.size = {
            w: w-2*this.padding,
            h: h-2*this.padding
        };
        this.size.hd2 = this.size.h / 2;

        this._calcNNPositions();
    }

    _calcNNPositions() {
        this.nnPositions = [
            this._calcNNPositionsForCol(0, this.nnDimmension.inputs),
            this._calcNNPositionsForCol(this.size.w/2, this.nnDimmension.hidden),
            this._calcNNPositionsForCol(this.size.w, this.nnDimmension.output)
        ];
    }

    _calcNNPositionsForCol(x, nnCount) {
        const res = [];
        const yStep = this.size.h / nnCount;
        for (let i = 0, y = yStep/2; i < nnCount; i++, y+=yStep)
            res.push({x: x, y: y});
        return res;
    }



    draw(car) {
        if (this.car === car)
            return;
        this.car = car;

        push();
        translate(this.offset.x, this.offset.y);
        this._performDrawing();
        pop();
    }

    _performDrawing() {
        fill(255);
        stroke(0,255,0);
        rect(-this.padding,-this.padding, this.size.w+this.padding*2, this.size.h+this.padding*2);

        fill("#727272");
        noStroke();
        for (const nnPosition of this.nnPositions) {
            for (const pos of nnPosition) {
                ellipse(pos.x, pos.y, this.radius, this.radius);
            }
        }
        let usedRows;
        usedRows = this._drawWeights(this.car.brain.weigths_hiddenOutput, this.nnPositions[1], this.nnPositions[2], false);
        usedRows = this._drawWeights(this.car.brain.weigths_inputHidden, this.nnPositions[0], this.nnPositions[1], usedRows);

    }

    _drawWeights(matrix, pos1, pos2, colsToUse) {
        const usedRows = [];
        for (let r = 0; r < matrix.rows; r++) {
            let isInUsed = false;
            for (let c = 0; c < matrix.cols; c++) {
                if (colsToUse)
                {
                    if (!colsToUse.includes(c))
                        continue;
                }
                const w = matrix.data[r][c];
                const p1 = pos1[r];
                const p2 = pos2[c];
                const color = this._getColor(w);
                if (color === 255)
                    continue;
                stroke(color);
                line(p1.x,p1.y, p2.x, p2.y);
                isInUsed = true;
            }
            if (isInUsed)
                usedRows.push(r);
        }
        return usedRows;
    }

    _getColor(w) {
        if (w < 0.2)
            return 255;
        if (w > 10)
            return 50;
        return p5.prototype.map(w, 0.2, 10, 100, 50);
    }
}
