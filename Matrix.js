class Matrix {
    constructor(rows, cols, data) {
        this.rows = rows;
        this.cols = cols;
        if (data)
            this.data = data;
        else
            this.data = new Array(this.rows).fill().map(() => new Array(this.cols).fill(0));
    }

    random(min=-1, max=1) {
        return this.map(e => Helper.random(min, max));
    }

    static sum(a, b) {
        return a.copy().add(b);
    }

    add(b) {
        if (b instanceof Matrix)
            return this.map((e, i, j) => e + b.data[i][j]);
        return this.map((e, i, j) => e + b)
    }

    static multiply(a, b) {
        return new Matrix(a.rows, b.cols).map((e,i,j) => {
            let sum = 0;
            for (let k = 0; k < a.cols; k++)
                sum += a.data[i][k] * b.data[k][j];
            return sum;
        });
    }

    map(func) {
        for (let i = 0; i < this.rows; i++)
            for (let j = 0; j < this.cols; j++)
                this.data[i][j] = func(this.data[i][j], i, j);

        return this;
    }

    copy() {
        let m = new Matrix(this.rows, this.cols);
        for (let i=0; i<this.rows; i++)
            for (let j=0; j<this.cols; j++)
                m.data[i][j] = this.data[i][j];
        return m;
    }

    static fromArray(array, asRow = true) {
        let m;
        if (asRow)
        {
            m = new Matrix(1, array.length);
            for (let i=0; i<m.cols; i++)
                m.data[0][i] = array[i];
        }
        else
        {
            m = new Matrix(array.length, 1);
            for (let i=0; i<m.rows; i++)
                m.data[i][0] = array[i];
        }
        return m;
    }

    toArray() {
        let arr = [];
        for (let i = 0; i < this.rows; i++)
            for (let j = 0; j < this.cols; j++)
                arr.push(this.data[i][j]);

        return arr;
    }

    print() {
        console.table(this.data);
    }
}