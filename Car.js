class Car {

    constructor(startPos, color) {
        this.pos = {
            x: startPos.x,
            y: startPos.y
        };
        this.angle = startPos.angle;

        this.speed = 0;
        this.turnAngle = 0;
        this.score = 0;

        this.broken = false;

        this.color = color;
        this.isBest = false;
    }

    draw() {
        push();
        translate(this.pos.x, this.pos.y);
        rotate(this.angle);
        this._performDrawing();
        pop();
    }

    _performDrawing() {
        this._drawCar();
    }

    _drawCar() {
        if (this.broken)
            stroke(255, 0, 0);
        else
            stroke(0);
        if (this.isBest)
        {
            fill("#00ff00");
            stroke("#00ff00");
        }
        else
            fill(this.color);

        beginShape();
        vertex(-10, -10);
        vertex(10, -10);
        vertex(20, 0);
        vertex(10, 10);
        vertex(-10, 10);
        endShape(CLOSE);
    }

    drive(gas, turnAngle) {
        if (this.broken)
            return;
        let newSpeed = this.speed + gas;
        newSpeed = constrain(newSpeed, 0, Car.MAX_SPEED);
        newSpeed *= 0.9;
        this.speed = newSpeed;

        turnAngle = constrain(turnAngle, -PI, PI);
        let normAngle = p5.prototype.map(turnAngle, -PI, PI, -Car.MAX_TURN_LIMIT_ANGLE, Car.MAX_TURN_LIMIT_ANGLE);
        let newAngle = this.turnAngle + normAngle;
        newAngle = constrain(newAngle, -Car.MAX_TURN_ANGLE, Car.MAX_TURN_ANGLE);
        newAngle *= 0.8;
        this.turnAngle = newAngle;

        this.angle += newAngle;
        this.pos.x += cos(this.angle) * newSpeed;
        this.pos.y += sin(this.angle) * newSpeed;

        this._calcScore();
    }

    _calcScore() {
        let k = 1;
        if (this.speed < 0)
            k *= 20;
        this.score += k*this.speed;
    }
}

Car.MAX_SPEED = 2;
// Car.MAX_TURN_ANGLE = 0.7853981633974483;//PI / 4
Car.MAX_TURN_ANGLE = 0.2617993877991494;//PI / 12
// Car.MAX_TURN_ANGLE = 0.06544984694978735;//PI / 48
Car.MAX_TURN_LIMIT_ANGLE = Car.MAX_TURN_ANGLE / 40;
