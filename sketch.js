let map;
let ga;
let defaultMaps = [];
let manualCar;
let framesCount = 0;
let carBrainConfig = null;
let rc;
let nnVisualizer;

const gaConfig = {
    rays:19,
    count:20,
    die:0.33,
    mutation:0.6,
}

function preload() {
    defaultMaps.push(loadJSON('maps/1.json'));
    defaultMaps.push(loadJSON('maps/2.json'));

    parseUrl();
}

function parseUrl() {
    const p = new URLSearchParams(window.location.search);
    if (p.has('r') && p.has('c') && p.has('d') && p.has('m'))
    {
        function f(k, a) {
            const v = + p.get(a);
            if (!isNaN(v))
                gaConfig[k] = v;
        }
        f('die', 'd');
        f('rays', 'r');
        f('count', 'c');
        f('mutation', 'm');
    }
    else if (p.has('b'))
    {
        const b = getBrains().find(e => e.id == p.get('b'));
        if (b)
        {
            gaConfig.rays = b.ga.rays;
            gaConfig.count = b.ga.count;
            gaConfig.die = b.ga.die;
            gaConfig.mutation = b.ga.mutation;
            carBrainConfig = b.brain;
        }
    }
    document.getElementById("ga_rays").value = gaConfig.rays;
    document.getElementById("ga_count").value = gaConfig.count;
    document.getElementById("ga_die").value = gaConfig.die;
    document.getElementById("ga_mutation").value = gaConfig.mutation;
}

function setup() {
    loadMapList();
    loadBrainList();
    const canvas = createCanvas(1600, 800);
    canvas.canvas.oncontextmenu = manualKill;
	map = new Map(defaultMaps[0]);
    manualCar = MakeCar('green');
	ga = new GeneticAlgorithm(gaConfig.count, gaConfig.die, gaConfig.mutation, carAICreationFunc);
	rc = new RayCast(1000, 0, 600, 400);
    nnVisualizer = new NNVisualizer(1000, 400, 600, 400, manualCar.brain);
    background(204);
}

function MakeCar(color) {
    return new CarAI(map.startPos, gaConfig.rays, color);
}

function draw() {
    if (map.checkCollision(manualCar))
        manualCar = MakeCar('green');
    manualMoveCar();
    manualCar.calcObstacleDistance(map);

    fill(204);
    rect(0,0,1000, 800);
    map.draw();

    if (framesCount++ >= 100)
    {
        framesCount = 0;
        let avg = 0;
        let isAllBroken = true;
        for (car of ga.items)
        {
            avg += car.score;
            if (car.broken)
                continue;
            let d = car.scoreDiff();
            if (d < 100){
                car.broken = true;
                continue;
            }
            isAllBroken = false;
        }
        avg /= ga.populationSize;
        document.getElementById('rtavgscore').innerHTML = 'Real time avg score: ' + avg.toFixed(5);

        if (isAllBroken)
        {
            ga.sortByScore();
            document.getElementById('bestscore').innerHTML = 'Best score: ' + ga.items[0].score.toFixed(5);
            document.getElementById('avgscore').innerHTML = 'Avg score: ' + avg.toFixed(5);
            console.log(ga.generation, ga.items[0].score, avg);
            document.getElementById('history').innerHTML =
                ga.generation + '&emsp;' + ga.items[0].score.toFixed(5) + '&emsp;' + avg.toFixed(5) + '&emsp;<br>' +
                document.getElementById('history').innerHTML;

            ga.createNewGeneration();
            document.getElementById('generation').innerHTML = 'Generation: ' + ga.generation;

        }
    }

    for (car of ga.items) {
        if (map.checkCollision(car))
            car.broken = true;
        car.calcObstacleDistance(map);
        car.driveAuto();
        car.draw();
    }

    manualCar.draw();

    const top = ga.topCar();
    top.draw()

    rc.draw(top);
    nnVisualizer.draw(top);
}

function manualMoveCar() {
    let gas = 0;
    let turn = 0;
    if (keyIsDown(LEFT_ARROW))
        turn = -3;
    if (keyIsDown(RIGHT_ARROW))
        turn = 3;
    if (keyIsDown(UP_ARROW))
        gas = 10;
    if (keyIsDown(DOWN_ARROW))
        gas = -10;

    manualCar.drive(gas, turn);
}

function killAll() {
    for (car of ga.items)
        car.broken = true;
}

function carAICreationFunc() {
    let carColor = color(Helper.randomInt(1, 255), Helper.randomInt(1, 255), Helper.randomInt(1, 255));
    let car = MakeCar(carColor);
    if (carBrainConfig)
        car.brain = NeuralNetwork.fromSavedConfig(carBrainConfig);
    return car;
}

setInterval(function () {
    document.title = 'fps: ' + frameRate().toFixed(2);
}, 1000);

function toggleCollapse(e, el) {
    el.parentElement.classList.toggle("collapsed");
}

function getMaps() {
    const mapStr = window.localStorage.getItem("maps");
    let maps = [];
    if (mapStr && mapStr.length){
        maps = JSON.parse(mapStr);
    }
    maps.unshift({title:"default 2",config:defaultMaps[1]});
    maps.unshift({title:"default 1",config:defaultMaps[0]});
    return maps;
}

function loadMapList() {
    const ul = document.getElementById("map_list");
    ul.innerHTML = '';
    let maps = getMaps();
    let i = -1;
    for (const mapConfig of maps) {
        i++;
        const index = i;
        const li = document.createElement("li");
        li.textContent = mapConfig.title;
        li.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
            let m = getMaps();
            killAll();
            map = new Map(m[index].config);

            return false;
        }
        ul.append(li);
    }
}

function applyGAConfig(e) {
    const r = document.getElementById("ga_rays").value;
    const c = document.getElementById("ga_count").value;
    const d = document.getElementById("ga_die").value;
    const m = document.getElementById("ga_mutation").value;
    window.location.href = `index.html?r=${r}&c=${c}&d=${d}&m=${m}`;
}

function getBrains() {
    const brainStr = window.localStorage.getItem("brains");
    if (!brainStr || brainStr.length === 0){
        window.localStorage.setItem("brains", "[]");
        return [];
    }
    return JSON.parse(brainStr);
}

function loadBrainList() {
    const ul = document.getElementById("brain_list");
    ul.innerHTML = '';
    let brains = getBrains();
    for (const brain of brains) {
        const brainId = brain.id;
        const a = document.createElement("a");
        a.textContent = "X";
        a.href = "#";
        a.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
            let bb = getBrains();
            const index = bb.findIndex(b=>b.id===brainId);
            bb.splice(index, 1);
            window.localStorage.setItem("brains", JSON.stringify(bb));
            loadBrainList();
            return false;
        }
        const li = document.createElement("li");
        li.textContent = brain.title;
        li.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();

            window.location.href = `index.html?b=${brainId}`;

            return false;
        }
        li.append(a);
        ul.append(li);
    }
}

function saveBrain(e) {
    e.preventDefault();
    e.stopPropagation();

    let title = prompt("brain_name");
    if (title.trim().length === 0)
    {
        alert("brain name cant be empty")
        return false;
    }
    const topCar = ga.topCar();
    const b = {
        title:title,
        id: Math.floor(random(10000, 99999)),
        brain: topCar.brain.save(),
        ga: {
            rays: topCar.raysCount,
            count: ga.populationSize,
            die: ga.diePercent,
            mutation: ga.mutationRate
        }
    }
    const bb = getBrains();
    bb.push(b);
    window.localStorage.setItem("brains", JSON.stringify(bb));

    loadBrainList();
    return false;
}

function manualKill(e) {
    e.stopPropagation();
    e.preventDefault();

    const maxDistance = 30*30;
    for (const item of ga.items) {
        const dx = item.pos.x - e.offsetX;
        const dy = item.pos.y - e.offsetY;
        const d2 =  dx*dx + dy*dy;
        if (d2 < maxDistance)
        {
            item.score = 0;
            item.broken = true;
            break;
        }
    }

    return false;
}
