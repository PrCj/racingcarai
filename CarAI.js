class CarAI extends Car {

    constructor(startPos, raysCount, color) {
        super(startPos, color);

        this.raysCount = raysCount;
        this.raysDistance = new Array(raysCount).fill(0);
        this.raysAngles = [];
        for (let i=0, rayAngle=-HALF_PI, rayOffset = PI / (raysCount-1); i<raysCount; i++, rayAngle+=rayOffset)
            this.raysAngles.push(rayAngle);

        this.brain = new NeuralNetwork(raysCount+2, raysCount*2, 2);

        this.lastCheckScore = 0;
    }

    scoreDiff() {
        if (this.score < 0)
            return -1000;
        let k = p5.prototype.map(this.score, 200, 400, 1, 4);
        let s = this.lastCheckScore;
        this.lastCheckScore = this.score;
        return (this.score - s) * k;
    }

    _performDrawing(){
        if (this.isBest && !this.broken)
            this._drawRays();
        super._performDrawing();
    }

    _drawRays() {
        stroke(0, 0, 255);
        for (let i=this.raysAngles.length; i--;) {
            push();
            rotate(this.raysAngles[i]);
            line(0, 0, this.raysDistance[i], 0);
            pop();
        }
    }

    calcObstacleDistance(map) {
        if (this.broken)
            return;
        let rays = [];
        for (const raysAngle of this.raysAngles) {
            const angle = this.angle + raysAngle + PI;
            let k = tan(angle);
            rays.push({
                angle: angle,
                k: k,
                x: this.pos.x,
                y: this.pos.y,
                b: this.pos.y-k*this.pos.x
            });
        }
        this.raysDistance = map.distanceCalc(rays);
    }

    driveAuto() {
        if (this.broken)
            return;
        let inputs = this.raysDistance;
        inputs.push(this.turnAngle);
        inputs.push(this.speed);
        let res = this.brain.predict(inputs);
        super.drive(res[0], res[1])
    }

    mutate(rate) {
        this.brain.mutate(rate);
    }
}
