class GeneticAlgorithm {

    constructor(count, diePercent, mutationRate, creationFunc) {
        this.items = [];
        this.populationSize = count;
        this.diePercent = diePercent;
        this.dieCount = Math.floor(count*diePercent);
        this.creationFunc = creationFunc;
        while(count--)
            this.items.push(creationFunc());
        this.mutationRate = mutationRate;
        this.generation = 0;
    }

    createNewGeneration() {
        this._removeBadItems();
        this._productNewItems();
        this._mutation();
        this.generation++;
    }

    topCar() {
        let c = this.items[0];
        for (const item of this.items) {
            item.isBest = false;
            if (item.score > c.score)
                c = item;
        }
        c.isBest = true;
        return c;
    }

    sortByScore() {
        this.items.sort((a, b) => b.score - a.score);
    }

    _removeBadItems() {
        this.items.splice(this.items.length-this.dieCount);
    }

    _productNewItems() {
        let newBrains = [];
        for (let item of this.items)
            newBrains.push(item.brain);

        function f(items, top1, top2) {
            const ai = Math.floor(random(0, top1));
            const bi = Math.floor(random(0, top2));
            let a = items[ai].brain;
            let b = items[bi].brain;
            newBrains.push(NeuralNetwork.crossover(a, b));
        }

        while (newBrains.length < this.populationSize - this.dieCount/2)
        {
            f(this.items, this.items.length/2, this.items.length/2);
        }

        while (newBrains.length < this.populationSize)
        {
            f(this.items, this.items.length/2, this.items.length);
        }

        this.items = [];
        for (let i=this.populationSize; i--;)
        {
            let item = this.creationFunc();
            item.brain = newBrains[i];
            this.items.push(item);
        }
    }

    _mutation() {
        let doFor = Math.floor(this.populationSize * 0.5);
        for (let item of this.items)
        {
            if (doFor-- <= 0)
                continue;
            if (Math.random() < this.mutationRate)
                item.mutate(this.mutationRate);
        }
    }
}
