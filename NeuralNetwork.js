class NeuralNetwork {
    constructor(a, b, c) {
        if (a)
        {
            this.weigths_inputHidden = new Matrix(a, b).random();
            this.bias_hidden = new Matrix(1, b).random();
            this.weigths_hiddenOutput = new Matrix(b, c).random();
            this.bias_output = new Matrix(1, c).random();
        }

        this.activationFunc = (x) => x;
        // this.activationFunc = (x) => 1 / (1 + Math.exp(-x));
    }

    setActivationFunc(func){
        this.activationFunc = func;
    }

    predict(inputs) {
        let inputMatrix = Matrix.fromArray(inputs);

        let hiddenRes = Matrix.multiply(inputMatrix, this.weigths_inputHidden);
        hiddenRes.add(this.bias_hidden).map(this.activationFunc);

        let outputRes = Matrix.multiply(hiddenRes, this.weigths_hiddenOutput);
        outputRes.add(this.bias_output).map(this.activationFunc);

        return outputRes.toArray();
    }

    copy() {
        let nn = new NeuralNetwork(this.weigths_inputHidden.rows, this.weigths_inputHidden.cols, this.weigths_hiddenOutput.cols);
        nn.weigths_inputHidden = this.weigths_inputHidden.copy();
        nn.weigths_hiddenOutput = this.weigths_hiddenOutput.copy();
        nn.bias_hidden = this.bias_hidden.copy();
        nn.bias_output = this.bias_output.copy();
        return nn;
    }

    mutate(rate) {
        function m(e) {
            return Math.random() < rate ? Helper.random(-30, 30) : e;
        }
        this.weigths_inputHidden.map(m);
        this.weigths_hiddenOutput.map(m);
        this.bias_hidden.map(m);
        this.bias_output.map(m);
    }

    static crossover(a, b) {
        let res = a.copy();
        res.weigths_hiddenOutput.map((e,i,j) => (e + b.weigths_hiddenOutput.data[i][j])/2);
        res.weigths_inputHidden.map((e,i,j) => (e + b.weigths_inputHidden.data[i][j])/2);
        res.bias_hidden.map((e,i,j) => (e + b.bias_hidden.data[i][j])/2);
        res.bias_output.map((e,i,j) => (e + b.bias_output.data[i][j])/2);
        return res;
    }

    static fromSavedConfig(config) {
        let n = new NeuralNetwork();
        n.weigths_inputHidden = new Matrix(config.weigths_inputHidden.rows, config.weigths_inputHidden.cols, config.weigths_inputHidden.data);
        n.weigths_hiddenOutput = new Matrix(config.weigths_hiddenOutput.rows, config.weigths_hiddenOutput.cols, config.weigths_hiddenOutput.data);
        n.bias_hidden = new Matrix(config.bias_hidden.rows, config.bias_hidden.cols, config.bias_hidden.data);
        n.bias_output = new Matrix(config.bias_output.rows, config.bias_output.cols, config.bias_output.data);
        return n;
    }

    save() {
        return {
            weigths_inputHidden: {
                rows: this.weigths_inputHidden.rows,
                cols: this.weigths_inputHidden.cols,
                data: this.weigths_inputHidden.data,
            },
            weigths_hiddenOutput: {
                rows: this.weigths_hiddenOutput.rows,
                cols: this.weigths_hiddenOutput.cols,
                data: this.weigths_hiddenOutput.data,
            },
            bias_hidden: {
                rows: this.bias_hidden.rows,
                cols: this.bias_hidden.cols,
                data: this.bias_hidden.data,
            },
            bias_output: {
                rows: this.bias_output.rows,
                cols: this.bias_output.cols,
                data: this.bias_output.data,
            },
        };
    }

}
