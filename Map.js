class Map {


    constructor(mapSettings, isEdit=false) {
        if (mapSettings)
        {
            this.borders = mapSettings.borders;

            if (!isEdit)
            {
                if (this.borders[0].length)
                    this.borders[0].push(this.borders[0][0]);
                if (this.borders[1].length)
                    this.borders[1].push(this.borders[1][0]);
            }
            this._prepareLines();

            this.startPos = mapSettings.startPos;
        }
        else
        {
            this.borders = [[],[]];
            this.startPos = {x:0,y:0,angle:0};
        }
    }

    checkCollision(car) {
        if (car.broken)
            return;
        let minDistance = 10;
        for (let border of this.borders)
            for (let i=border.length-1; i>0;i--)
				if (this._distanceToSegment(border[i-1], border[i], car.pos) <= minDistance)
					return true;
        return false;
    }

    _distanceToSegment(p1, p2, p) {
        let m = (p.x - p1.x)*(p2.x-p1.x)+(p.y-p1.y)*(p2.y-p1.y);
        let n = Math.pow(p2.x-p1.x, 2)+Math.pow(p2.y-p1.y, 2);
        let t = m/n;

        if (t<0)
            return sqrt(sq(p1.x-p.x)+sq(p1.y-p.y));
        else if (t>1)
            return sqrt(sq(p2.x-p.x)+sq(p2.y-p.y));

        let a = (p2.y - p1.y);
        let b = (p1.x - p2.x);
        let c = (p2.x*p1.y-p1.x*p2.y);
        let d = abs(a*p.x + b*p.y + c) / sqrt(a*a + b*b);
        return abs(d);
    }

    _prepareLines() {
        this.borderLines = [];
        for (let border of this.borders)
			for (let i=border.length-1; i>0;i--)
				this.borderLines.push(this._createLineEquation(border[i-1], border[i]));
    }

    _createLineEquation(a, b) {
        let c = b.x-a.x;
        let k = c == 0 ? 0 : (b.y-a.y)/c;
        return {
            k: k,
            b: b.y - k*b.x,
            p1: a,
            p2: b,
        }
    }

    distanceCalc(rays) {
        let res = [];
        for (let ray of rays) {
            let min = 10000000000;
            for (let borderLine of this.borderLines) {
                let dist = this._distanceToSection(borderLine, ray);
                if (dist > 0 && dist < min)
                    min = dist;
            }
            res.push(sqrt(min));
        }
        return res;
    }

    _distanceToSection(borderLineEq, rayEq) {
        let c = rayEq.k - borderLineEq.k;
        if (c == 0)
            return -1;
		let x = (borderLineEq.b - rayEq.b) / c;
        let p = {
			x: x,
			y: borderLineEq.k * x + borderLineEq.b
		};

        if (abs(borderLineEq.p1.x - p.x) + abs(borderLineEq.p2.x - p.x) - abs(borderLineEq.p2.x - borderLineEq.p1.x) > Number.EPSILON
            || abs(borderLineEq.p1.y - p.y) + abs(borderLineEq.p2.y - p.y) - abs(borderLineEq.p2.y - borderLineEq.p1.y) > Number.EPSILON)
            return -1;

		let k = 5;
		let newP = {
			x: cos(rayEq.angle)*k + rayEq.x,
			y: sin(rayEq.angle)*k + rayEq.y
		};

		if ((p.x - rayEq.x) * (newP.x - rayEq.x) > 0 || (p.y - rayEq.y) * (newP.y - rayEq.y) > 0)
			return -1;

        return sq(p.x - rayEq.x) + sq(p.y - rayEq.y);
    }

    draw() {
        noFill();
        stroke(0);

        for (let border of this.borders)
		{
			beginShape();
			for (let v of border)
				vertex(v.x, v.y);
			endShape();
		}
    }

    getConfig() {
        return {
            startPos: this.startPos,
            borders: this.borders
        };
    }
}
