let map;

function setup() {
    const canvas = createCanvas(1000, 800);
    canvas.canvas.onclick = addPoint;
    canvas.canvas.oncontextmenu = removePoint;
    map = new MapEditor();

    loadMapList();
}

function draw() {
    background(204);
    map.draw();
}

function changeActiveBorder(borderIndex) {
    map.activeBorder = borderIndex;
}

function addPoint(e) {
    if (map.activeBorder !== -1)
        map.addPoint(e.offsetX, e.offsetY);
    else
    {
        map.startPos.x = e.offsetX;
        map.startPos.y = e.offsetY;
    }

    return false;
}

function removePoint(e) {
    if (map.activeBorder !== -1)
        map.removePoint();
    else
        map.startPos.angle = Math.atan2(e.offsetY-map.startPos.y, e.offsetX-map.startPos.x);

    return false;
}

function saveMap(event) {
    const title = prompt("map name", "");
    if (title.trim().length === 0)
    {
        alert("map name can't be empty");
        return false;
    }
    let maps = getMaps();
    maps.push({title: title, config: map.getConfig()});
    window.localStorage.setItem("maps", JSON.stringify(maps));

    loadMapList();
}

function getMaps()
{
    const mapStr = window.localStorage.getItem("maps");
    if (!mapStr || mapStr.length === 0){
        window.localStorage.setItem("maps", "[]");
        return [];
    }
    return JSON.parse(mapStr);
}

function setStartPoint() {
    map.activeBorder = -1
}

function loadMapList() {
    const ul = document.getElementById("map_list");
    ul.innerHTML = '';
    let maps = getMaps();
    let i = -1;
    for (const mapConfig of maps) {
        i++;
        const index = i;
        const a = document.createElement("a");
        a.textContent = "X";
        a.href = "#";
        a.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
            let m = getMaps();
            m.splice(index, 1);
            window.localStorage.setItem("maps", JSON.stringify(m));
            loadMapList();
            return false;
        }
        const li = document.createElement("li");
        li.textContent = mapConfig.title;
        li.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
            let m = getMaps();
            map = new MapEditor(m[index].config);
            return false;
        }
        li.append(a);
        ul.append(li);
    }
}
