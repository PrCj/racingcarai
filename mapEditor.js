class MapEditor extends Map {
    constructor(mapSettings) {
        super(mapSettings, true);
        this.activeBorder = 0;
    }

    drawExtraLine(activeBorder) {
        const other = this.borders[activeBorder === 0 ? 1 : 0];
        if (other.length>=2)
        {
            const l = other[other.length-1];
            line(other[0].x, other[0].y, l.x, l.y);
        }
    }

    drawStartVector() {
        if (this.startPos.x === 0 && this.startPos.y === 0 && this.startPos.angle === 0)
            return;

        push();
        translate(this.startPos.x, this.startPos.y);
        rotate(this.startPos.angle);

        fill("#009000");
        ellipse(0, 0, 8, 8);
        line(0, 0, 50, 0)
        line(50, 0, 40, 7)
        line(50, 0, 40, -7)
        pop();
    }

    draw() {
        Map.prototype.draw.call(this);

        this.drawStartVector();

        if (this.activeBorder < 0){
            this.drawExtraLine(0);
            this.drawExtraLine(1);
            return;
        }

        this.drawExtraLine(this.activeBorder);

        const activeBorder = this.borders[this.activeBorder];
        if (!activeBorder.length)
            return;
        const firstPoint = activeBorder[0];
        noStroke();
        fill("#ff0000");
        ellipse(firstPoint.x, firstPoint.y, 8, 8);

        if (activeBorder.length < 2)
            return;

        const lastPoint = activeBorder[activeBorder.length-1];
        fill("#009000");
        ellipse(lastPoint.x, lastPoint.y, 8, 8);

        if (activeBorder.length < 3)
            return;
        stroke("#ff0000");
        line(firstPoint.x, firstPoint.y, lastPoint.x, lastPoint.y);

    }

    addPoint(x, y) {
        this.borders[this.activeBorder].push({x:x, y:y});
    }

    removePoint() {
        const b = this.borders[this.activeBorder];
        if (b.length)
            b.pop();
    }
}
