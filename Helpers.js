class Helper {
    static random(min=-1, max=1) {
        return Math.random() * (max - min) + min;
    }

    static randomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}
